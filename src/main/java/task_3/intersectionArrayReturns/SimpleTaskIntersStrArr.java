package task_3.intersectionArrayReturns;

public interface SimpleTaskIntersStrArr {

    /**
     * Returns the intersection of two string arrays.  The resulting array will contain only the strings that are
     * present in both arrays and will be in the order that each string is encountered in array1.
     */
    String[] getIntersection(String[] array1, String[] array2);
}
