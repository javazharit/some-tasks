package task_3.intersectionArrayReturns;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestRunnerInterStrArr {

    SimpleTaskIntersStrArr test = new SimpleTaskInterStrArrImpl();

    @Test
    public void testGetIntersection() {
        String[] array1 = new String[]{"Asdf"};
        String[] array2 = new String[]{"asdf"};
        String[] results = test.getIntersection(array1, array2);

        assertEquals("Results", Arrays.toString(new String[0]), Arrays.toString(results));
        assertEquals("Original array1 was modified", Arrays.toString(new String[]{"Asdf"}), Arrays.toString(array1));
        assertEquals("Original array2 was modified", Arrays.toString(new String[]{"asdf"}), Arrays.toString(array2));

        results = test.getIntersection(new String[]{"a", "b", "c", "d", "e"}, new String[]{"A", "1", "a", "z"});
        assertEquals("Results", Arrays.toString(new String[]{"a"}), Arrays.toString(results));

        results = test.getIntersection(new String[]{"c", "b", "e", "d", "a"}, new String[]{"d", "c", "e", "z"});
        assertEquals("Results", Arrays.toString(new String[]{"c", "e", "d"}), Arrays.toString(results));

        results = test.getIntersection(new String[]{"a", "b", "c", "d", "e"}, new String[]{"A", "1", "b", "a", "z"});
        assertEquals("Results", Arrays.toString(new String[]{"a", "b"}), Arrays.toString(results));

        results = test.getIntersection(new String[]{"a", "b", "a", "d", "e"}, new String[]{"A", "1", "b", "a", "z"});
        assertEquals("Results", Arrays.toString(new String[]{"a", "b"}), Arrays.toString(results));

        results = test.getIntersection(new String[]{"a", "b", "a", "d", "e", "f", "h", "j"}, new String[]{"b"});
        assertEquals("Results", Arrays.toString(new String[]{"b"}), Arrays.toString(results));

    }
}
