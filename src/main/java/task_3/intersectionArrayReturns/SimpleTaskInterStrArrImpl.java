package task_3.intersectionArrayReturns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimpleTaskInterStrArrImpl implements SimpleTaskIntersStrArr {

    @Override
    public String[] getIntersection(String[] inputArr1, String[] inputArr2) {

        List<String> inputArr1List = new ArrayList<>();
        List<String> inputArr2List = new ArrayList<>();

        Collections.addAll(inputArr1List, inputArr1);
        Collections.addAll(inputArr2List, inputArr2);

        inputArr1List.retainAll(inputArr2List);
        int count = 0;
        for (int i = 0; i < inputArr1List.size(); i++) {
            for (int j = 0; j < inputArr1List.size(); j++) {
                if (inputArr1List.get(i).equals(inputArr1List.get(j))) {
                    count++;
                    if (count >= 2) {
                        inputArr1List.remove(j);
                    }
                }
            }
            count = 0;
        }
        return inputArr1List.toArray(new String[inputArr1List.size()]);
    }
}