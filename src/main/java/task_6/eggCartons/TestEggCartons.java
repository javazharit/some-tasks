package task_6.eggCartons;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestEggCartons {

    EggCartons test = new EggCartonsImpl();

    @Test
    public void testCalculateNumberOfEggCartons() {
        assertEquals("Failed with 1 eggs", -1, test.calculateNumberOfEggCartons(1));
        assertEquals("Failed with 2 eggs", -1, test.calculateNumberOfEggCartons(2));
        assertEquals("Failed with 3 eggs", -1, test.calculateNumberOfEggCartons(3));
        assertEquals("Failed with 4 eggs", -1, test.calculateNumberOfEggCartons(4));
        assertEquals("Failed with 5 eggs", -1, test.calculateNumberOfEggCartons(5));
        assertEquals("Failed with 6 eggs", 1, test.calculateNumberOfEggCartons(6));
        assertEquals("Failed with 7 eggs", -1, test.calculateNumberOfEggCartons(7));
        assertEquals("Failed with 8 eggs", 1, test.calculateNumberOfEggCartons(8));
        assertEquals("Failed with 12 eggs", 2, test.calculateNumberOfEggCartons(12));
        assertEquals("Failed with 15 eggs", -1, test.calculateNumberOfEggCartons(15));
        assertEquals("Failed with 16 eggs", 2, test.calculateNumberOfEggCartons(16));
        assertEquals("Failed with 24 eggs", 3, test.calculateNumberOfEggCartons(24));
        assertEquals("Failed with 28 eggs", 4, test.calculateNumberOfEggCartons(28));
        assertEquals("Failed with 20 eggs", 3, test.calculateNumberOfEggCartons(20));
        assertEquals("Failed with 30 eggs", 4, test.calculateNumberOfEggCartons(30));
        assertEquals("Failed with 32 eggs", 4, test.calculateNumberOfEggCartons(32));
        assertEquals("Failed with 34 eggs", 5, test.calculateNumberOfEggCartons(34));
        assertEquals("Failed with 36 eggs", 5, test.calculateNumberOfEggCartons(36));
        assertEquals("Failed with 38 eggs", 5, test.calculateNumberOfEggCartons(38));
        assertEquals("Failed with 40 eggs", 5, test.calculateNumberOfEggCartons(40));
        assertEquals("Failed with 42 eggs", 6, test.calculateNumberOfEggCartons(42));
        assertEquals("Failed with 52 eggs", 7, test.calculateNumberOfEggCartons(52));
        assertEquals("Failed with 58 eggs", 8, test.calculateNumberOfEggCartons(58));
        assertEquals("Failed with 60 eggs", 8, test.calculateNumberOfEggCartons(60));
        assertEquals("Failed with 144 eggs", 18, test.calculateNumberOfEggCartons(144));
        assertEquals("Failed with 80006 eggs", 10001, test.calculateNumberOfEggCartons(80006));
    }
}
