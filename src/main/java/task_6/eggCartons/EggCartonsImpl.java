package task_6.eggCartons;

public class EggCartonsImpl implements EggCartons {
    @Override
    public int calculateNumberOfEggCartons(int numberOfEggs) {
        int simpleValue;

        for (int i = 0; i < numberOfEggs; i++) {
            for (int j = 0; j < numberOfEggs; j++) {
                simpleValue = 6 * i + 8 * j;
                if (simpleValue == numberOfEggs) {
                    return i + j;
                }
            }
        }

        return -1;
    }
}
