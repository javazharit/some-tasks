package task_6.eggCartons;


public interface EggCartons {

    /**
     * There are two sizes of egg cartons. One size contains 6 eggs and the other contains 8 eggs.  This function
     * accepts a number of eggs and returns the smallest number of cartons required to contain that many eggs.
     * Each carton must be completely filled.
     *
     * for example:
     * 20 eggs will need 3 cartons,  2 cartons containing 6 eggs and 1 carton containing 8 eggs.
     * 24 eggs will need 3 cartons,  3 cartons containing 8 eggs.
     * 15 eggs returns  -1 cartons,  because you can't return 15 eggs with only cartons of size 8 and 6.
     * 34 eggs returns   5 cartons,  3 cartons containing 6 eggs and 2 cartons containing 8 eggs.
     */
    int calculateNumberOfEggCartons(int numberOfEggs);

}
