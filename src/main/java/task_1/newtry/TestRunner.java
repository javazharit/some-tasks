package task_1.newtry;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestRunner {

    SimpleTasks tasks = new SimpleTaskImpl();

    @Test
    public void testReverseDouble() {
        assertEquals("Failed", 90.125, tasks.reverseNumber(521.09));
        assertEquals("Failed!", 321.0, tasks.reverseNumber(12300));
        assertEquals("Failed!", 9999.0, tasks.reverseNumber(9999));
        assertEquals("Failed!", 0.1, tasks.reverseNumber(1.0));
        assertEquals("Failed!", -0.1, tasks.reverseNumber(-1.0));
        assertEquals("Failed!", 0.111, tasks.reverseNumber(111.0));
        assertEquals("Failed!", 123.01, tasks.reverseNumber(10.321));
        assertEquals("Failed!", 12300.0, tasks.reverseNumber(0.00321));
        assertEquals("Failed!", -12300.0, tasks.reverseNumber(-0.00321));
        assertEquals("Failed!", 12300.03, tasks.reverseNumber(30.00321));
    }
}
