package task_1.newtry;

public class SimpleTaskImpl implements SimpleTasks {
    private double reverseNumber;
    private int number;
    private String inputNumStr;
    private String reverseStr = "";

    @Override
    public double reverseNumber(Number inputNumber) {

        String inputNumberStr = String.valueOf(inputNumber);
        String inputNumberFractionStr =
                inputNumberStr.substring(inputNumberStr.indexOf('.') + 1, inputNumberStr.length());

        /**
         * check on fractional part, integer or double,
         * if the fractional part is missing, it means that the input number is an integer,
         * the number of input method returns the input value(Integer).
        * */
        if (inputNumberFractionStr.equals(inputNumberStr)) {
            //** check on the integer number in the plus or minus sign
            if (inputNumber.intValue() > 0) {
                getReverseIntNumberPlusType(inputNumber.intValue());
                reverseStr = "";
            } else {
                getReverseIntNumberMinusType(inputNumber.intValue());
                reverseStr = "";
            }
        } else
            // check on the double number in the plus or minus sign
            if (inputNumber.doubleValue() > 0) {
            getReverseDoubleNumberPlusType(inputNumber.doubleValue());
            reverseStr = "";
        } else {
            getReverseDoubleNumberMinusType(inputNumber.doubleValue());
            reverseStr = "";
        }
        return reverseNumber;
    }

    private double getReverseIntNumberPlusType(int inputNumber) {
        number = inputNumber;
        inputNumStr = String.valueOf(number);
        for (int i = inputNumStr.length() - 1; i >= 0; i--) {
            reverseStr += inputNumStr.charAt(i);
        }
       return reverseNumber = Integer.parseInt(reverseStr);
    }

    private double getReverseIntNumberMinusType(int inputNumber) {
        number = -1 * inputNumber;
        inputNumStr = String.valueOf(number);
        for (int i = inputNumStr.length() - 1; i >= 0; i--) {
            reverseStr += inputNumStr.charAt(i);
        }
        return reverseNumber = -1 * Integer.parseInt(reverseStr);
    }

    private double getReverseDoubleNumberPlusType(double inputNumber) {
        inputNumStr = String.valueOf(inputNumber);
        for (int i = inputNumStr.length() - 1; i >= 0; i--) {
            reverseStr += inputNumStr.charAt(i);
        }
        return reverseNumber = Double.parseDouble(reverseStr);
    }

    private double getReverseDoubleNumberMinusType(double inputNumber) {
        inputNumber = -1 * inputNumber;
        inputNumStr = String.valueOf(inputNumber);
        for (int i = inputNumStr.length() - 1; i >= 0; i--) {
            reverseStr += inputNumStr.charAt(i);
        }
        return reverseNumber = (-1) * Double.parseDouble(reverseStr);
    }
}


