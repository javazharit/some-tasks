package task_7.subArray;

public interface MaxSubArraySum {

    /**
     * Given an array of integers.
     * Need to find subarray within a given array for which the sum of elements is the largest.
     * Subarray can contain one or more elements.
     *
     * Example array [-3,1,-3,4,−1,3,1,−6,4] the subarray [4,−1,3,1] and the sum is 7.
     */
    int maxSubArraySum(int[] array);

}
