package task_7.subArray;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMaxSubArraySum {

    MaxSubArraySum test = new MaxSubArraySumImpl();

    @Test
    public void testMaxSubArraySum() {
        assertEquals("Wrong max!", 6, test.maxSubArraySum(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
        assertEquals("Wrong max!", 7, test.maxSubArraySum(new int[]{-3, 1, -3, 4, -1, 3, 1, -6, 4}));
        assertEquals("Wrong max!", 1, test.maxSubArraySum(new int[]{1}));
        assertEquals("Wrong max!", -1, test.maxSubArraySum(new int[]{-1}));
        assertEquals("Wrong max!", 0, test.maxSubArraySum(new int[]{0}));
        assertEquals("Wrong max!", 1, test.maxSubArraySum(new int[]{-1, 1}));
        assertEquals("Wrong max!", 2, test.maxSubArraySum(new int[]{-1, 1, -1, 0, -1, -1, -1, 2, -2}));
        assertEquals("Wrong max!", 0, test.maxSubArraySum(new int[]{}));
    }
}
