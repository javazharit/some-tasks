package task_7.subArray;

public class MaxSubArraySumImpl implements MaxSubArraySum {
    public int maxSubArraySum(int[] array) {

        int sum;
        int maximumComp = 0;

        // when an array containing one element
        if (array.length == 1) {
            for (Integer num : array) {
                maximumComp = num;
            }
        }

        // when an array containing one element
        if (array.length == 1) {
            for (Integer num : array) {
                maximumComp = num;
            }
        }

        for (int j = 0; j < array.length; j++) {
            sum = 0;
            // input array's: finding a maximum sum of the array
            for (Integer value : array) {
                if (value > maximumComp) {
                    maximumComp = value;
                }
            }

            for (int i = j; i < array.length; i++) {
                sum += array[i];
                if (sum > maximumComp) {
                    maximumComp = sum;
                }
            }
        }

        return maximumComp;
    }
}
