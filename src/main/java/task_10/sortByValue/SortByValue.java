package task_10.sortByValue;

import java.util.Map;

public interface SortByValue {

    /**
     * Sort a map by value.
     * All the values are the same type objects
     * <p/>
     * For example if the input is {["a": 10], ["c": 5], ["b": 15]}, this function returns {["c": 5], ["a": 10], ["b": 15]}
     * another example is the result map {["5": "a"], ["2": "b"], ["20": "c"]} for the
     * input map {["20": "c"], ["2": "b"], ["5": "a"]}
     *
     * @param source
     * @param <K>    type of the keys
     * @param <V>    type of the values
     * @return
     */
    <K, V> Map<K, V> sortByValue(Map<K, V> source);

}
