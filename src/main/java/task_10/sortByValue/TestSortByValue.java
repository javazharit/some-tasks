package task_10.sortByValue;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

public class TestSortByValue {

    SortByValue test = new SortByValueImpl();

    @Test
    public void testSortByValue() {
        assertEquals("Failed for null!", null, test.sortByValue(null));

        HashMap<String, Double> inputMap = new HashMap<>();
        inputMap.put("a", 10.5);
        assertEquals("Failed for single entry!", inputMap, test.sortByValue(inputMap));

        HashMap<String, Double> inputMapforDouble = new HashMap<>();
        inputMapforDouble.put("a", 10.5);
        inputMapforDouble.put("b", 1.0);
        inputMapforDouble.put("c", 2.3);
        inputMapforDouble.put("d", 4.1);

        String[] stringKeys = new String[]{"b", "c", "d", "a"};
        Double[] doubleValues = new Double[]{1.0, 2.3, 4.1, 10.5};

        int i = 0;
        for (Map.Entry<String, Double> entry : test.sortByValue(inputMapforDouble).entrySet()) {
            assertEquals("Failed, key is wrong!", stringKeys[i], entry.getKey());
            assertEquals("Failed, value is wrong!", doubleValues[i], entry.getValue());
            i++;
        }

        HashMap<Integer, String> inputMapforString = new HashMap<>();
        inputMapforString.put(1, "r");
        inputMapforString.put(2, "b");
        inputMapforString.put(10, "a");
        inputMapforString.put(11, "c");
        inputMapforString.put(3, "A");

        Integer[] intKeys = new Integer[]{3, 10, 2, 11, 1};
        String[] stringValues = new String[]{"A", "a", "b", "c", "r"};

        i = 0;
        for (Map.Entry<Integer, String> entry : test.sortByValue(inputMapforString).entrySet()) {
            assertEquals("Failed, key is wrong!", intKeys[i], entry.getKey());
            assertEquals("Failed, value is wrong!", stringValues[i], entry.getValue());
            i++;
        }
    }
}
