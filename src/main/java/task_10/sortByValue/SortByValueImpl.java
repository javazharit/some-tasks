package task_10.sortByValue;

import java.util.*;

public class SortByValueImpl implements SortByValue {

    @Override
    public <K, V> Map<K, V> sortByValue(final Map<K, V> source) {

        if (source == null){
            return null;
        }

        Map<K, V> inputMap = new TreeMap<>(source);

        // swap the K and V type
        Map<V, K> reverseKeyValueMap = new TreeMap<>();
        for (Map.Entry<K, V> entry : inputMap.entrySet()) {
            reverseKeyValueMap.put(entry.getValue(), entry.getKey());
        }

        // swap the V and K type
        Map<K, V> finalMap = new LinkedHashMap<>();
        for (Map.Entry<V, K> entry : reverseKeyValueMap.entrySet()) {
            finalMap.put(entry.getValue(), entry.getKey());
        }

        return finalMap;
    }
}
