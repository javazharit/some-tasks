package task_4.TwoUniqueChars;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestRunnerTwoUniqueChars {

    SimpleRunnerTwoUniqueChars test = new SimpleRunnerTwoUniqueCharsImpl();

    @Test
    public void testGetLongestSubWithoutRepeat() {
        assertEquals("Wrong substring!", "bcbbbbcccb", test.findLongestSubstringWithTwoUniqueChars("abcbbbbcccbdddadacb"));

//        assertEquals("Wrong substring!", "", test.findLongestSubstringWithTwoUniqueChars(""));
//
//        assertEquals("Wrong substring!", "1122", test.findLongestSubstringWithTwoUniqueChars("1234561122334567"));
//
//        assertEquals("Wrong substring!", "ll11", test.findLongestSubstringWithTwoUniqueChars("123456null112233null4567"));
//
//        assertEquals("Wrong substring!", "fffffffffffffffffffffff",
//                test.findLongestSubstringWithTwoUniqueChars("fffffffffffffffffffffff"));
//
//        assertEquals("Wrong substring!", "1ffffffffffffffffffffff1f",
//                test.findLongestSubstringWithTwoUniqueChars("1ffffffffffffffffffffff1f"));
//
//        assertEquals("Wrong substring!", "                   ",
//                test.findLongestSubstringWithTwoUniqueChars("                   "));
//
//        assertEquals("Wrong substring!", "12",
//                test.findLongestSubstringWithTwoUniqueChars("123456789012345678901234567890"));
//
//        assertEquals("Wrong substring!", "你好",
//                test.findLongestSubstringWithTwoUniqueChars("你好我的朋友"));
    }
}
