package task_4.TwoUniqueChars;

public interface SimpleRunnerTwoUniqueChars {

    /**
     * From source string need to find the longest substring which contains no more than two unique characters.
     * For example, given "aebeeeebbbedddadacb", the longest substring that contains 2 unique characters is "ebeeeebbbe".
     * Return the first substring in case there exist more than one with equal length.
     */
    String findLongestSubstringWithTwoUniqueChars(String source);
}
