package task_4.TwoUniqueChars;

public class SimpleRunnerTwoUniqueCharsImpl implements SimpleRunnerTwoUniqueChars {

    @Override
    public String findLongestSubstringWithTwoUniqueChars(String inputSourceStr) {
        int maxlength = 0;
        int count = 0;

        String tempSubString;
        String stringAllValues;
        String outputSubString = "";

        StringBuilder stringBuilder = new StringBuilder(inputSourceStr.length());

        while (count + 2 < inputSourceStr.length()) {
            int i = 0;
            char previousChar = inputSourceStr.charAt(count);
            char nextChar = inputSourceStr.charAt(count + 1);
            stringBuilder.append(previousChar);
            stringBuilder.append(nextChar);

            while ((previousChar == nextChar) && (count + 2 < inputSourceStr.length())) {
                count++;
                nextChar = inputSourceStr.charAt(count + 1);
                stringBuilder.append(nextChar);
            }

            tempSubString = inputSourceStr.substring(count + 2);
            while (i < tempSubString.length()) {
                if (tempSubString.charAt(i) == previousChar || tempSubString.charAt(i) == nextChar) {
                    stringBuilder.append(tempSubString.charAt(i));
                    i++;
                } else
                    break;
            }
            stringAllValues = stringBuilder.toString();
            if (maxlength < stringAllValues.length()) {
                maxlength = stringAllValues.length();
                outputSubString = stringAllValues;
            }
            count++;
            stringBuilder.setLength(0);
        }
        return outputSubString;
    }
}
