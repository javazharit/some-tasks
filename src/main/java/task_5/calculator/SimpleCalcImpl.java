package task_5.calculator;

import java.util.ArrayList;
import java.util.List;

public class SimpleCalcImpl implements SimpleCalc {

    @Override
    public double calculate(String source) {
        String inputStr;
        String result = "";
        String added = "+";
        String subtraction = "-";
        double operationValue = 0;

        List<Double> statmentList = new ArrayList<>();
        List<String> symbList = new ArrayList<>();

        /**
         * extracting double from a string
         */
        for (int i = 0; i < source.length(); i++) {
            inputStr = String.valueOf(source.charAt(i));
            if ((inputStr.equals(added)) || (inputStr.equals(subtraction))) {
                symbList.add(inputStr);
                statmentList.add(Double.valueOf(result));
                result = "";
            } else {
                result += inputStr;
                if (i == (source.length()) - 1) {
                    statmentList.add(Double.valueOf(result));
                }
            }
        }

        /**
         * block where the involved arithmetic functions, initialization and selection of functions
        */
        for (int i = 0; i < symbList.size(); i++) {
            if (symbList.get(i).equals(added)) {
                operationValue = statmentList.get(i) + statmentList.get(i + 1);
                statmentList.set(i + 1, operationValue);
            }
            if (symbList.get(i).equals(subtraction)) {
                operationValue = statmentList.get(i) - statmentList.get(i + 1);
                statmentList.set(i + 1, operationValue);
            }
        }

        return operationValue;
    }
}
