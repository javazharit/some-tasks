package task_5.calculator;

public interface SimpleCalc {
    double calculate(String statement);
}
