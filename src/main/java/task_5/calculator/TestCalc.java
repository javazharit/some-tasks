package task_5.calculator;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestCalc {
    SimpleCalc test = new SimpleCalcImpl();

    @Test
    public void testCalculate() {
        assertEquals(22.0, test.calculate("1+20+1"));
//        assertEquals(2.5, test.calculate("1.5+1"));
//        assertEquals(19.0, test.calculate("9+10"));
//
//        assertEquals(0.0, test.calculate("1-1"));
//        assertEquals(0.5, test.calculate("1.5-1"));
//        assertEquals(-1.0, test.calculate("9-10"));
//
//        assertEquals(1.0, test.calculate("1+1-1"));
//        assertEquals(0.0, test.calculate("1.5-1-.5"));
//        assertEquals(9.0, test.calculate("9-10+5+5"));
//        assertEquals(9.0, test.calculate("9+1+1+1+1+1+3-7-1"));
    }
}
