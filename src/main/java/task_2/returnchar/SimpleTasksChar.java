package task_2.returnchar;


public interface SimpleTasksChar {

    /**
     * Return repeating characters in a given string in the same order as they appear.
     * For example if the input is "Mary had a little lamb" the result set should be {'a', 'l', 't'}
     */
    char[] findDuplicatesInString(String source);
}
