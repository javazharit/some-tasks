package task_2.returnchar;

public class SimpleTasksCharImpl implements SimpleTasksChar {

    @Override
    public char[] findDuplicatesInString(String inputSource) {
        int count = 0;
        String buffer = "";
        String compareSrc = inputSource;

        if (inputSource == null)
            return null;

        for (int i = 0; i < inputSource.length(); i++) {
            String input = String.valueOf(inputSource.charAt(i));
            for (int j = 0; j < compareSrc.length(); j++) {
                String compare = String.valueOf(compareSrc.charAt(j));
                if (input.equals(compare)) {
                    count++;
                    if (count >= 2 && count < 3) {
                        buffer += input;
                        for (int k = 0; k < compareSrc.length(); k++) {
                            compareSrc = compareSrc.replace(input, "");
                        }
                    }
                }
            }
            count = 0;
        }

        return buffer.toCharArray();
    }
}