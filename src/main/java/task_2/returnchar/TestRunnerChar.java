package task_2.returnchar;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestRunnerChar {
    char[] source;
    char[] array;

    SimpleTasksChar test = new SimpleTasksCharImpl();

    @Test
    public void testFindDuplicatesInString() {
        assertEquals("Failed!", null, test.findDuplicatesInString(null));

        source = test.findDuplicatesInString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        array = new char[0];
        assertEquals("Failed", source.length, array.length);
        for (int i = 0; i < source.length; i++) {
            assertEquals(source[i], array[i]);
        }

        source = test.findDuplicatesInString("Mary had a little lamb");
        array = new char[]{'a', ' ', 'l', 't'};
        assertEquals("Failed", source.length, array.length);
        for (int i = 0; i < source.length; i++) {
            assertEquals(source[i], array[i]);
        }

        source = test.findDuplicatesInString("Happy Birthday!");
        array = new char[]{'a', 'p', 'y'};
        assertEquals("Failed", source.length, array.length);
        for (int i = 0; i < source.length; i++) {
            assertEquals(source[i], array[i]);
        }

        source = test.findDuplicatesInString("Wow!, That was Huge, John!");
        array = new char[]{'o', 'w', '!', ',', ' ', 'h', 'a'};
        assertEquals("Failed", source.length, array.length);
        for (int i = 0; i < source.length; i++) {
            assertEquals(source[i], array[i]);
        }
    }
}

