package task_8.primeSum;

public interface PrimeSum {

    /**
     * Write a function that returns a Sum of the M'th and N'th prime numbers.
     * For example if the m=4 and n=7 then this function returns 24 (7 + 17).
     */
    int getPrimesSum(int m, int n);

}
