package task_8.primeSum;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestPrimeSumImpl {

    PrimeSumImpl test = new PrimeSumImpl();

    @Test
    public void testGetPrimesSum() {
        assertEquals("Failed!", 0, test.getPrimesSum(0, 0));
        assertEquals("Failed!", 4, test.getPrimesSum(1, 1));
        assertEquals("Failed!", 17, test.getPrimesSum(-5, 7));
        assertEquals("Failed!", 24, test.getPrimesSum(4, 7));
        assertEquals("Failed!", 23, test.getPrimesSum(0, 9));
        assertEquals("Failed!", 33, test.getPrimesSum(1, 11));
        assertEquals("Failed!", 386, test.getPrimesSum(50, 37));
        assertEquals("Failed!", 112648, test.getPrimesSum(10000, 1000));
    }
}
