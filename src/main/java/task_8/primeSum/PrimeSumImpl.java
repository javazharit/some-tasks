package task_8.primeSum;

public class PrimeSumImpl implements PrimeSum {

    @Override
    public int getPrimesSum(int m, int n) {
        int numForPrimes;
        int count = 0;
        int primeNum;
        int primeCount = 0;
        int valueN = 0;
        int valueM = 0;

        if (n > m) {
            numForPrimes = 10 * n;
        } else {
            numForPrimes = 10 * m;
        }

        if (n > 1000 || m > 1000) {
            if (n > m) {
                numForPrimes = 12 * n;
            } else {
                numForPrimes = 12 * m;
            }
        }

        for (int i = 2; i <= numForPrimes; i++) {
            for (int j = 1; j <= numForPrimes; j++) {
                if ((i % j) == 0) {
                    count++;
                }
            }
            if (count < 3) {
                primeNum = i;
                primeCount++;
                if (primeCount == n) {
                    valueN = primeNum;
                }
                if (primeCount == m) {
                    valueM = primeNum;
                }
            }
            count = 0;
        }

        return (valueN + valueM);
    }
}

