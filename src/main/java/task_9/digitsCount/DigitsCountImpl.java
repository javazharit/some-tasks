package task_9.digitsCount;

import java.util.HashMap;
import java.util.Map;

public class DigitsCountImpl implements DigitsCount {

    @Override
    public Map<Integer, Integer> getDigitsCount(int inputNum) {
        int count0 = 0;
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        int count5 = 0;
        int count6 = 0;
        int count7 = 0;
        int count8 = 0;
        int count9 = 0;
        int compareNum;

        String str;
        String countStr;

        Map<Integer, Integer> numberCountsMap = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            numberCountsMap.put(i, 0);
        }

        for (int i = 1; i <= inputNum; i++) {
            str = String.valueOf(i);

            for (int j = 0; j < str.length(); j++) {
                countStr = String.valueOf(str.charAt(j));
                compareNum = Integer.parseInt(countStr);

                switch (compareNum) {
                    case 0:
                        count0++;
                        numberCountsMap.put(0, count0);
                        break;
                    case 1:
                        count1++;
                        numberCountsMap.put(1, count1);
                        break;
                    case 2:
                        count2++;
                        numberCountsMap.put(2, count2);
                        break;
                    case 3:
                        count3++;
                        numberCountsMap.put(3, count3);
                        break;
                    case 4:
                        count4++;
                        numberCountsMap.put(4, count4);
                        break;
                    case 5:
                        count5++;
                        numberCountsMap.put(5, count5);
                        break;
                    case 6:
                        count6++;
                        numberCountsMap.put(6, count6);
                        break;
                    case 7:
                        count7++;
                        numberCountsMap.put(7, count7);
                        break;
                    case 8:
                        count8++;
                        numberCountsMap.put(8, count8);
                        break;
                    case 9:
                        count9++;
                        numberCountsMap.put(9, count9);
                        break;
                    default:
                        System.out.println("Nothing compared");
                        break;
                }
            }
        }

        return numberCountsMap;
    }
}
