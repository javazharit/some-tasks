package task_9.digitsCount;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

public class TestDigitsCount {

    DigitsCount test = new DigitsCountImpl();

    @Test
    public void testGetDigitsCount() {

        Map<Integer, Integer> result = new HashMap<>();
        result.put(0, 0);
        result.put(1, 1);
        result.put(2, 1);
        result.put(3, 1);
        result.put(4, 1);
        result.put(5, 1);
        result.put(6, 1);
        result.put(7, 1);
        result.put(8, 1);
        result.put(9, 0);
        assertEquals("Failed!", result, test.getDigitsCount(8));

        result = new HashMap<>();
        result.put(0, 2);
        result.put(1, 13);
        result.put(2, 9);
        result.put(3, 3);
        result.put(4, 3);
        result.put(5, 3);
        result.put(6, 2);
        result.put(7, 2);
        result.put(8, 2);
        result.put(9, 2);
        assertEquals("Failed!", result, test.getDigitsCount(25));

        result = new HashMap<>();
        result.put(0, 113327);
        result.put(1, 224438);
        result.put(2, 159114);
        result.put(3, 119114);
        result.put(4, 114114);
        result.put(5, 113438);
        result.put(6, 113413);
        result.put(7, 113333);
        result.put(8, 113327);
        result.put(9, 113327);
        assertEquals("Failed!", result, test.getDigitsCount(234675));

        result = new HashMap<>();
        result.put(0, 1367945);
        result.put(1, 2479055);
        result.put(2, 1825806);
        result.put(3, 1425806);
        result.put(4, 1375806);
        result.put(5, 1369046);
        result.put(6, 1368796);
        result.put(7, 1367996);
        result.put(8, 1367945);
        result.put(9, 1367945);
        assertEquals("Failed!", result, test.getDigitsCount(2346750));

        result = new HashMap<>();
        result.put(0, 2617284);
        result.put(1, 3728395);
        result.put(2, 3728395);
        result.put(3, 3728395);
        result.put(4, 3111115);
        result.put(5, 2617284);
        result.put(6, 2617284);
        result.put(7, 2617284);
        result.put(8, 2617284);
        result.put(9, 2617284);
        assertEquals("Failed!", result, test.getDigitsCount(4444444));
    }
}
