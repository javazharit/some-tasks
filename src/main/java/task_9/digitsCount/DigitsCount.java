package task_9.digitsCount;

import java.util.Map;

public interface DigitsCount {

    /**
     * Write a program that takes a positive number (N) as input and returns a map
     * which contains digits from 0 to 9 as keys, and number of occurrence of each
     * digit in all integers from 1 to N.
     *
     * @param num the positive number N
     * @return the map with each numbers count (from 0 to 9) in all integers from 1 to N
     */
    Map<Integer, Integer> getDigitsCount(int num);

}
